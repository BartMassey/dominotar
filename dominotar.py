#!/usr/bin/python3
# Copyright (c) 2016 Bart Massey
# Generate domino tiling problems with unique solutions

from copy import deepcopy
from sys import argv, stdin
from random import randrange

class Dirn():
    def __init__(self, desc):
        self.desc = desc

down = Dirn("|V")
across = Dirn("->")

class Domino():
    def __init__(self, row, col, dirn):
        self.row = row
        self.col = col
        self.dirn = dirn

    def loc(self):
        if self.dirn == down:
            return ((self.row, self.col), (self.row + 1, self.col))
        elif self.dirn == across:
            return ((self.row, self.col), (self.row, self.col + 1))
        else:
            assert False

class Board():
    def __init__(self, rows, cols, blocks=None):
        self.rows = rows
        self.cols = cols
        self.dsquares = dict()
        for r in range(rows):
            for c in range(cols):
                self.dsquares[(r, c)] = '.'
        self.dominos = dict()
        if blocks:
            for b in blocks:
                self.block(b)

    def print(self):
        for r in range(self.rows):
            for c in range(self.cols):
                print(self.dsquares[(r, c)], end = "")
            print()

    def lay(self, domino):
        (start, end) = domino.loc()
        (end_row, end_col) = end
        if end_row >= self.rows or end_col >= self.cols:
            return False
        if self.dsquares[start] != '.' or self.dsquares[end] != '.':
            return False
        desc = domino.dirn.desc
        self.dsquares[start] = desc[0]
        self.dsquares[end] = desc[1]
        self.dominos[start] = domino
        self.dominos[end] = domino
        return True

    def unlay(self, domino):
        (start, end) = domino.loc()
        self.dsquares[start] = '.'
        del self.dominos[start]
        self.dsquares[end] = '.'
        del self.dominos[end]

    def block(self, c):
        assert self.dsquares[c] == '.'
        self.dsquares[c] = '#'

    def unblock(self, c):
        assert self.dsquares[c] == '#'
        self.dsquares[c] = '.'

    def solve(self):
        for r in range(self.rows):
            for c in range(self.cols):
                if self.dsquares[(r, c)] != '.':
                    continue
                dd = Domino(r, c, down)
                da = Domino(r, c, across)
                for d in [dd, da]:
                    if self.lay(d):
                        result = self.solve()
                        self.unlay(d)
                        if result:
                            return result
                return None
        return deepcopy(self)

    def open(self, c):
        return self.dsquares[c] == '.'

def read_board(f):
    lines = f.readlines()
    rows = len(lines)
    cols = len(lines[0].strip())
    board = Board(rows, cols)
    for r in range(rows):
        for c in range(cols):
            if lines[r][c] == '.':
                continue
            elif lines[r][c] == '#':
                board.block((r, c))
            else:
                assert False
    return board

def solve(b):
    answer = b.solve()
    if answer:
        answer.print()
    else:
        print("no solution for")
        b.print()
    print()


def test():
    boards = [
        Board(4, 4, [(0, 0), (3, 0), (1, 3), (2, 3)]),
        Board(6, 6, [(0, 0), (5, 0), (2, 5), (3, 5),
                     (2, 2), (2, 3), (3, 2), (3, 3)]) ]

    for b in boards:
        solve(b)

def gen_board(rows, cols):
    def coord(b):
        while True:
            r = randrange(rows - 2) + 1
            c = randrange(cols - 2) + 1
            if b.open((r, c)):
                return (r, c)

    def color(coord):
        r, c = coord
        return (r % 2) ^ (c % 2)
        
    b = Board(rows, cols)
    while True:
        c1 = coord(b)
        c2 = None
        while True:
            c2 = coord(b)
            if color(c1) != color(c2):
                break
        b.block(c1)
        b.block(c2)
        if not b.solve():
            b.unblock(c1)
            b.unblock(c2)
            return b

if len(argv) <= 1:
    b = read_board(stdin)
    solve(b)
elif len(argv) == 2 and argv[1] == "test":
    test()
elif len(argv) == 2:
    f = open(argv[1], "r")
    b = read_board(f)
    solve(b)
elif len(argv) == 3:
    rows = int(argv[1])
    cols = int(argv[2])
    b = Board(rows, cols)
    solve(b)
elif len(argv) == 4 and argv[1] == "gen":
    rows = int(argv[2])
    cols = int(argv[3])
    b = gen_board(rows, cols)
    b.print()
else:
    assert False
